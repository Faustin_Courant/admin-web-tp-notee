import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../../model/user';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private postUrl = "http://localhost:8080/utilisateur/add";
  private getById = "http://localhost:8080/utilisateur/";
  private getByMail = "http://localhost:8080/utilisateur/mail/";
  private putUpdate = "http://localhost:8080/utilisateur/";
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  
  constructor(private http:HttpClient)
  {

  }
  
  addUser(utilisateur : User):Observable<User>
  {
    return this.http.post<User>(this.postUrl,utilisateur,this.httpOptions);
  }

  getUser(id:number)
  {
    return this.http.get<User>(this.getById+id);
  }

  updateUser(user: User)
  {
    return this.http.put<User>(this.putUpdate,user,this.httpOptions);
  }
}
