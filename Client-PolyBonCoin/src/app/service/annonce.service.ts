import { Injectable } from '@angular/core';
import { Annonce } from '../model/annonce';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AnnonceService {
  private dataUrl = "http://localhost:8080/annonce";
  private getById = "http://localhost:8080/annonce/";
  private postUrl = "http://localhost:8080/annonce/add";
  private estAMoiURL = "http://localhost:8080/annonce/estAMoi/";
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  
  constructor(private http:HttpClient)
  {

  }

  getAnnonces() : Observable<Annonce[]>
  {
    return this.http.get<Annonce[]>(this.dataUrl);
  }

  getAnnonceById(id:number) : Observable<Annonce>
  {
    return this.http.get<Annonce>(this.getById+id);
  }
  
  ajouterAnnonce(annonce : Annonce):Observable<Annonce>
  {
    return this.http.post<Annonce>(this.postUrl,annonce,this.httpOptions);
  }

  estAMoi(id:number):Observable<boolean>
  {
    return this.http.get<boolean>(this.estAMoiURL+id);
  }
}
