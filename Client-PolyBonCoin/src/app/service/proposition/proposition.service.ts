import { Injectable } from '@angular/core';
import { Proposition } from 'src/app/model/proposition';
import { Observable } from 'rxjs';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PropositionService {
  postUrl:string = "http://localhost:8080/proposition/add";
  getURL:string = "http://localhost:8080/proposition/an/";
  getURLuser:string = "http://localhost:8080/proposition/test/";
  setReponseURL:string = "http://localhost:8080/proposition/reponse/";
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  
  constructor(private http:HttpClient) { }

  createProposition(proposition:Proposition) : Observable<Proposition>
  {
    return this.http.post<Proposition>(this.postUrl,proposition,this.httpOptions);
  }
  getPropositionAnnonce(idAnnonce:number)
  {
    return this.http.get<Proposition[]>(this.getURL+idAnnonce);
  }
  getPropositionAnnonceUser(idAnnonce:number)
  {
    return this.http.get<Proposition[]>(this.getURLuser+idAnnonce);
  }

  setReponse(idProposition:number,states:number)
  {
      return this.http.put<Proposition>(this.setReponseURL+idProposition,{reponse:states},this.httpOptions);
  }

}
