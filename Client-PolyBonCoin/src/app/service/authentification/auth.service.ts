import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/model/user';
import { shareReplay, tap } from 'rxjs';
import * as moment from "moment";
import { Authentification } from 'src/app/model/authentification';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient,
    private router: Router) { }

  login(email:string, password:string )
  {
    return this.http.post<Authentification>('http://localhost:8080/utilisateur/login', {ut_email : email, ut_pwd: password})
        .pipe(shareReplay(1))
        .pipe(tap(res => this.setSession(res)))
        ;
        // this is just the HTTP call, 
        // we still need to handle the reception of the token
  }

  private setSession(authResult:Authentification)
  {
    const expiresAt = moment().add(authResult.expiresIn,'second');
    localStorage.setItem('id_token', authResult.accesToken);
    localStorage.setItem('id_user', JSON.stringify(authResult.userID));
    localStorage.setItem('refreshToken', authResult.refreshToken);
    localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()));
    
  } 

  public logout()
  {
    localStorage.removeItem("id_token");
    localStorage.removeItem("refreshToken");
    localStorage.removeItem("expires_at");
    localStorage.removeItem("id_user");
    //TODO Redirection
    this.router.navigateByUrl("/");
  }      

  public isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
      return !this.isLoggedIn();
  }

  getExpiration()
  {
    const expiration = localStorage.getItem("expires_at");
    if(expiration)
    {
      const expiresAt = JSON.parse(expiration);
      return moment(expiresAt);
    }
    else
      return null;
  }
  
  getUserId(): string
  {
    return localStorage.getItem("id_user") || "";
  }
}
