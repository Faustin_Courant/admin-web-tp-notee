import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';

import { AuthService } from '../service/authentification/auth.service';
import { Router } from '@angular/router';
import { NotificationService } from '../service/notification/notification.service'
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    form:FormGroup;
    returnUrl: string;

    constructor(private fb:FormBuilder, 
                 private route: ActivatedRoute,
                 private authService: AuthService, 
                 private router: Router,
                 private notify : NotificationService) {

        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        this.form = this.fb.group({
            email: ['',Validators.required],
            password: ['',Validators.required]
        });
    }

  ngOnInit(): void {
    console.log("Route Save : "+ this.route.snapshot.queryParams['returnUrl']);
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login()
  {
    const val = this.form.value;

    if (val.email && val.password) {
        this.authService.login(val.email, val.password)
            .subscribe(
                () => {
                    console.log("User is logged in");
                    this.notify.showSuccess("Bonjours !", "");
                    this.router.navigateByUrl(this.returnUrl);
                },
                (err) => {
                  this.notify.showError("User ou mots de passe inconnu", "Veuillez réessayer !");
                }
            );
    }
  }

}
