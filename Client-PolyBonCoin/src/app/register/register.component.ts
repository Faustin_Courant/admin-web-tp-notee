import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { NotificationService } from '../service/notification/notification.service';
import { UserService } from '../service/user/user.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  newUser:User = new User(0,"","","","","");

  constructor(private userService: UserService,
              private notifyService : NotificationService,
              private router: Router) {}

  ngOnInit(): void {
  }
  addUser()
  {
    this.userService.addUser(this.newUser).subscribe(
      user => {
        this.notifyService.showSuccess("Bienvenu sur le PolyBonCoin !","Vous pouvez maintenant accéder a tout le site");
        this.newUser = new User(0,"","","","","");
        this.router.navigateByUrl("/login")
      },
      err => {
        this.notifyService.showError("Un problème est survenu","ERR: " +err.status+" Veuillez réessayer !");
      }
    )
  }

}
