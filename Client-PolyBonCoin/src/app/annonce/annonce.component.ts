import { Component, Input, OnInit } from '@angular/core';
import { Annonce } from '../model/annonce';
import { AnnonceService } from '../service/annonce.service';
import { ActivatedRoute, NavigationEnd } from '@angular/router';
import { AuthService } from '../service/authentification/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-annonce',
  templateUrl: './annonce.component.html',
  styleUrls: ['./annonce.component.css']
})
export class AnnonceComponent implements OnInit {

  annonces : Annonce[] = [];
  annonces_save : Annonce[] = [];
  mode: string = "default";
  search:string ="";

  constructor(private annonceService : AnnonceService,
              private auth: AuthService,
              private route: ActivatedRoute,
              private router: Router) {
                    router.events.subscribe((val) => {
                      // see also 
                      if(val instanceof NavigationEnd)
                      {
                        console.log("URL  :"+ val.url);
                        this.mode = this.route.snapshot.queryParams['mode'] || 'default';
                        this.search = this.route.snapshot.queryParams['search'] || '';
                        this.refreshFilter();
                      }
                      console.log("EVENT :" + (val instanceof NavigationEnd));
                  });
                  this.mode = this.route.snapshot.queryParams['mode'] || 'default';
                  console.log("Mode : " +this.mode);
                  
               }

  getAnnonces(): void
  {
    this.annonceService.getAnnonces().subscribe(msg =>{
      this.annonces = msg;
      this.annonces_save = this.annonces;
      this.refreshFilter();
    } );
  }

  refreshFilter()
  {
    if(this.mode == "mesAnnonces")
    {
      let id = parseInt(this.auth.getUserId());
      console.log("ID  : " + id);
      this.annonces = this.annonces_save.filter(annonce => { 
          console.log("utAnn : "+ annonce.utilisateurID + " id : " + id);
          return annonce.utilisateurID == id;})
      this.annonces.forEach(annonce => {

      });    
    }
    if(this.mode == "search")
    {
      this.annonces = this.annonces_save.filter(annonce => { 
        return annonce.titre.toLowerCase().includes(this.search.toLowerCase());})
        console.log("Search");
    }
  }

  ngOnInit(): void {
    this.getAnnonces();
  }

}
