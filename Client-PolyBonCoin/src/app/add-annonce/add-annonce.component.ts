import { Component, OnInit } from '@angular/core';
import { Annonce } from '../model/annonce';
import { AnnonceService } from '../service/annonce.service';
import { NotificationService } from '../service/notification/notification.service';
@Component({
  selector: 'app-add-annonce',
  templateUrl: './add-annonce.component.html',
  styleUrls: ['./add-annonce.component.css']
})
export class AddAnnonceComponent implements OnInit {

  newAnnonce = new Annonce(1,"","",0.0,2);
  constructor(private annonceService : AnnonceService,private notifyService : NotificationService)
  {}

  addAnnonce()
  {
    this.annonceService.ajouterAnnonce(this.newAnnonce).subscribe(
      annonce => {
        console.log(annonce);
        this.notifyService.showSuccess("Ajout réussi", "Votre annonce à bien été ajouter");
        this.newAnnonce = new Annonce(1,"","",0.0,2); //Reset du form
      },
      error => {
          console.log(error.status);
          this.notifyService.showError("Annonce pas ajouter : ", "Il y a un probleme err :" + error.status);
      }
    )
  }

  ngOnInit(): void {
  }

}
