import { Component } from '@angular/core';
import { AuthService } from './service/authentification/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'PolyBonCoin';
  addIcone='assets/icon/add_icon.png';
  user_icon= "assets/icon/user.png";

  searchBar:string ="";


  constructor(public auth: AuthService,
    private router:Router)
  {}

  mesAnnonces()
  {
    this.router.navigate(['/annonces'],{queryParams:{mode:"mesAnnonces"}});
  }

  searchClick()
  {    
    this.router.navigate(['/annonces'],{queryParams:{mode:"search",search:this.searchBar}});
  }

  search()
  {
    console.log("Search url : " + this.router.url);
    if(this.router.url.startsWith("/annonces") )
    {
      this.router.navigate(['/annonces'],{queryParams:{mode:"search",search:this.searchBar}});
    }
  }
}
