import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MypropositionComponent } from './myproposition.component';

describe('MypropositionComponent', () => {
  let component: MypropositionComponent;
  let fixture: ComponentFixture<MypropositionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MypropositionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MypropositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
