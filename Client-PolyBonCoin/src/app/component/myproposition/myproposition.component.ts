
import { Component, Input, OnInit } from '@angular/core';

import { Proposition } from 'src/app/model/proposition';
import { PropositionService } from 'src/app/service/proposition/proposition.service';

@Component({
  selector: 'app-myproposition',
  templateUrl: './myproposition.component.html',
  styleUrls: ['./myproposition.component.css']
})
export class MypropositionComponent implements OnInit {

  propositions : Proposition[] = [];
  @Input() annonceID?:number;
  @Input() utilisateurID?:number;

  constructor(private propositionService:PropositionService) {}

  
  ngOnInit(): void {
    if(this.annonceID && this.utilisateurID)
    {
      this.propositionService.getPropositionAnnonceUser(this.annonceID).subscribe( resultProp =>
        {
          this.propositions = resultProp;
        });
    }
  }

}
