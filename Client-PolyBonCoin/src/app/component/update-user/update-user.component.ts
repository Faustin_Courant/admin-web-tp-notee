import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/service/user/user.service';
import { NotificationService } from 'src/app/service/notification/notification.service';
import { AuthService } from 'src/app/service/authentification/auth.service';
@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  user:User = new User(0,"","","","","");

  constructor(private auth:AuthService,
              private notif:NotificationService,
              private userService:UserService)
  {
    console.log("Constructeur");
    userService.getUser(parseInt(auth.getUserId())).subscribe(
      user => {
        this.user = user;
        console.log("User are updated");
        console.log(JSON.stringify(user));
      }
    )
  }

  ngOnInit(): void {
  }

  updateUser()
  {
    this.userService.updateUser(this.user).subscribe(
      user => {
        this.notif.showSuccess("Reussi","Majréussi");
      },
      err =>{
        this.notif.showError("Echec","Echec")
      }
    )
  }

}
