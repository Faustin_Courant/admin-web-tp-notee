import { Component, Input, OnInit } from '@angular/core';
import { Proposition } from 'src/app/model/proposition';
import { PropositionService } from 'src/app/service/proposition/proposition.service';

@Component({
  selector: 'app-proposition-annonce',
  templateUrl: './proposition-annonce.component.html',
  styleUrls: ['./proposition-annonce.component.css']
})
export class PropositionAnnonceComponent implements OnInit {
  @Input() annonceID?:number;
  propositions : Proposition[] = [];
  constructor(private propositionService:PropositionService) {}
  vendu:boolean = false;

  ngOnInit(): void {
    if(this.annonceID)
      this.propositionService.getPropositionAnnonce(this.annonceID).subscribe( resultProp =>
        {
          this.propositions = resultProp;

          this.propositions.forEach(proposition => {
            if(proposition.status == 2)
            {
              this.vendu = true;
            }
          });
        });
  }

  reponseProposition(idProposition:number,rep:number)
  {
    this.propositionService.setReponse(idProposition,rep).subscribe(msg => {
      console.log("Reponse set");
    })
    window.location.reload();
  }

}
