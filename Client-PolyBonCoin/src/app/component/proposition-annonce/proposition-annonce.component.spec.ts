import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PropositionAnnonceComponent } from './proposition-annonce.component';

describe('PropositionAnnonceComponent', () => {
  let component: PropositionAnnonceComponent;
  let fixture: ComponentFixture<PropositionAnnonceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PropositionAnnonceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PropositionAnnonceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
