import { Component, OnInit } from '@angular/core';;
import { Annonce } from 'src/app/model/annonce';
import { ActivatedRoute } from '@angular/router';
import { AnnonceService } from 'src/app/service/annonce.service';
import { NotificationService } from 'src/app/service/notification/notification.service';
import { AuthService } from 'src/app/service/authentification/auth.service';
@Component({
  selector: 'app-detail-annonce',
  templateUrl: './detail-annonce.component.html',
  styleUrls: ['./detail-annonce.component.css']
})
export class DetailAnnonceComponent implements OnInit {

  annonce:Annonce = new Annonce(0,"","",0,0);
  id:number = 0;
  display:boolean = false;
  modeProprio: boolean = false;
  utilisateurID:number = 0;


  constructor(private _Activatedroute:ActivatedRoute,
              private _annonceService:AnnonceService,
              private _authService:AuthService,
              private _notify:NotificationService)             
  {
    let _id = this._Activatedroute.snapshot.paramMap.get("id");

    if(_id)
      this.id=parseInt(_id);
  }

  ngOnInit(): void {
    this._annonceService.getAnnonceById(this.id).subscribe(msg =>{
      this.annonce = msg;
      console.log("Annonce good");
   });

   this._annonceService.estAMoi(this.id).subscribe(estAmoi =>{
      this.modeProprio = estAmoi;
   })

   this.utilisateurID = parseInt(this._authService.getUserId());

  }

  displaytrue()
  {
    this.display = true;
  }

  displayEvent(result:string)
  {
    console.log("res : " + result);
    if(result == "Annuler")
    {
      this.display = false;
    }
    else if (result == "Valider")
    {
      this.display = false;
      this._notify.showSuccess("Proposition envoyer","Vous aurez bientôt une réponse");
    }
    else if(result == "Erreur")
    {
      this.display = true;
      this._notify.showError("Veuillez réessayer ! ","Ouuppps un problème est apparu");
    }


  }

}
