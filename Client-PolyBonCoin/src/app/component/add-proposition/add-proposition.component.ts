import { Component, Input, OnInit } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';

import { Proposition } from 'src/app/model/proposition';
import { PropositionService } from 'src/app/service/proposition/proposition.service';
@Component({
  selector: 'app-add-proposition',
  templateUrl: './add-proposition.component.html',
  styleUrls: ['./add-proposition.component.css']
})
export class AddPropositionComponent implements OnInit {

  proposition:Proposition = new Proposition();
  @Input() annonceID?:number;
  @Output() result = new EventEmitter<string>();
  
  constructor(private propositionService:PropositionService) { }

  ngOnInit(): void {
    if(this.annonceID)
      this.proposition.annonceID = this.annonceID;
  }

  annuler() {
    this.result.emit("Annuler");
  }

  valider()
  {
    if(this.annonceID)
      this.proposition.annonceID = this.annonceID;
    this.propositionService.createProposition(this.proposition).subscribe(
      msg => 
      {
        this.result.emit("Valider");
      },
      err => {
        this.result.emit("Erreur");
      })
      window.location.reload();
  }


}
