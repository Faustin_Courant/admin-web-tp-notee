import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../service/authentification/auth.service';
import { NotificationService } from '../service/notification/notification.service'

@Injectable({
  providedIn: 'root'
})
export class LogAccessGuard implements CanActivate {

  constructor(private auth:AuthService,private notif:NotificationService,private router:Router){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree 
  {
    const res: boolean = this.auth.isLoggedIn();
    if(!res)
    {
      this.router.navigate(['/login'],{queryParams:{returnUrl:state.url}});
      this.notif.showError("Accèss reserver au membre","Veuillez vous connecter");
    }
    return res;
  }


  
}
