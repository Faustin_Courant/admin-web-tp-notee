import { TestBed } from '@angular/core/testing';

import { LogAccessGuard } from './log-access.guard';

describe('LogAccessGuard', () => {
  let guard: LogAccessGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(LogAccessGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
