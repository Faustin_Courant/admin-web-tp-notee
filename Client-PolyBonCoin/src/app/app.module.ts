import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AnnonceComponent } from './annonce/annonce.component';
import { AddAnnonceComponent } from './add-annonce/add-annonce.component';

import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UpdateUserComponent } from './component/update-user/update-user.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { JwtInterceptor } from './interceptor/jwt.interceptor';
import { DetailAnnonceComponent } from './component/detail-annonce/detail-annonce.component';
import { AddPropositionComponent } from './component/add-proposition/add-proposition.component';
import { PropositionAnnonceComponent } from './component/proposition-annonce/proposition-annonce.component';
import { MypropositionComponent } from './component/myproposition/myproposition.component';

@NgModule({
  declarations: [
    AppComponent,
    AnnonceComponent,
    AddAnnonceComponent,
    LoginComponent,
    RegisterComponent,
    UpdateUserComponent,
    DetailAnnonceComponent,
    AddPropositionComponent,
    PropositionAnnonceComponent,
    MypropositionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
