import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnnonceComponent } from './annonce/annonce.component';
import { AddAnnonceComponent } from './add-annonce/add-annonce.component';
import { LoginComponent } from './login/login.component';
import { LogAccessGuard } from './guard/log-access.guard';
import { RegisterComponent } from './register/register.component';
import { UpdateUserComponent } from "./component/update-user/update-user.component";
import { DetailAnnonceComponent } from './component/detail-annonce/detail-annonce.component';

const routes : Routes = [
  {path: "annonces", component:AnnonceComponent},
  {path: "add-annonce", component:AddAnnonceComponent, canActivate:[LogAccessGuard]},
  {path: "annonceDetail/:id",component:DetailAnnonceComponent, canActivate:[LogAccessGuard]},
  {path: "login", component:LoginComponent},
  {path: "register", component:RegisterComponent},
  {path: "updateUser", component:UpdateUserComponent, canActivate:[LogAccessGuard]},
  {path:"",redirectTo:"annonces",pathMatch:"full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

