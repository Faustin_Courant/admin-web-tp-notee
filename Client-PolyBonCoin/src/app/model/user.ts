export interface IUser
{
    id: number;
    email: string;
    nom: string;
    prenom: string;
    password: string;
    password_crf: string;
}

export class User implements IUser
{
    id: number;
    email: string;
    nom: string;
    prenom: string;
    password: string;
    password_crf: string;

    constructor( id: number,
                 email: string,
                 nom: string,
                 prenom: string,
                 password: string,
                 password_crf: string)
    {
        if(id)
            this.id = id;
        else
            this.id = 0;
        if(email)
            this.email = email;
        else
            this.email = "";
        if(nom)
            this.nom = nom;
        else
            this.nom = "";
        if(prenom)
            this.prenom = prenom;
        else
            this.prenom = "";
        if(password)
            this.password=password;
        else
            this.password = "";
        if(password_crf)
            this.password_crf = password_crf;
        else
            this.password_crf = "";
    }
}