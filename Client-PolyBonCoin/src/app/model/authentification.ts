export interface IAuthentification
{
    accesToken: string;
    refreshToken: string;
    expiresIn: string;
    userID : number;
}

export class Authentification implements IAuthentification
{
    accesToken: string;
    refreshToken: string;
    expiresIn: string;
    userID : number;

    constructor( accesToken: string,
                refreshToken: string,
                expiresIn: string,
                userID : number)
    {
        if(accesToken)
            this.accesToken = accesToken;
        else
            this.accesToken = "";
        if(refreshToken)
            this.refreshToken = refreshToken;
        else
            this.refreshToken = "";
        if(expiresIn)
            this.expiresIn = expiresIn;
        else
            this.expiresIn = "";
        if(userID)
            this.userID = userID;
        else
            this.userID = 0;
    }
}