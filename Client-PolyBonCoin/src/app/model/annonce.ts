export interface IAnnonce
{
    id: number;
    titre: string;
    description: string;
    prix: number;
    utilisateurID:number;
    createdAt: string;
    updatedAt: string;
}

export class Annonce implements IAnnonce
{
    id: number;
    titre: string;
    description: string;
    prix: number;
    utilisateurID:number;
    createdAt: string;
    updatedAt: string;

    constructor( id: number, titre: string, 
                description: string, prix: number,
                 utilisateurID: number )
    {
        if(id)
            this.id = id;
        else
            this.id = 0;
        if(titre)
            this.titre = titre;
        else
            this.titre = "";
        if(description)
            this.description = description;
        else
            this.description = "";
        if(prix)
            this.prix = prix;
        else
            this.prix = 0;
        if(utilisateurID)
            this.utilisateurID = utilisateurID;
        else
            this.utilisateurID = 0;

        this.createdAt = "";
        this.updatedAt = "";
    }
}