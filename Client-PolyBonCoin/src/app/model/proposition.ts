export interface IProposition
{
    id: number;
    utilisateurID:number;
    annonceID : number;
    prix : number;
    status: number;
    createdAt: string;
    updatedAt: string;
}

export class Proposition implements IProposition
{
    id              :number = 0;
    utilisateurID   :number = 0;
    annonceID       :number = 0;
    prix            :number = 0;
    status          :number = 0;
    createdAt       :string = "";
    updatedAt       :string = "";

    constructor()
    {
    }
}