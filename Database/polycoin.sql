-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 09 nov. 2021 à 08:59
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `polycoin`
--

-- --------------------------------------------------------

--
-- Structure de la table `annonce`
--

DROP TABLE IF EXISTS `annonce`;
CREATE TABLE IF NOT EXISTS `annonce` (
  `an_id` int(11) NOT NULL AUTO_INCREMENT,
  `an_titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `an_description` text COLLATE utf8_unicode_ci NOT NULL,
  `an_prix` double NOT NULL,
  `ut_id` int(11) NOT NULL,
  PRIMARY KEY (`an_id`),
  KEY `UT_AN_FOREIGN` (`ut_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `proposition`
--

DROP TABLE IF EXISTS `proposition`;
CREATE TABLE IF NOT EXISTS `proposition` (
  `an_id` int(11) NOT NULL,
  `ut_id` int(11) NOT NULL,
  `prix` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `pr_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`pr_id`),
  KEY `UT_PR_FOREIGN` (`ut_id`),
  KEY `AN_PR_FOREIGN` (`an_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `ut_id` int(11) NOT NULL AUTO_INCREMENT,
  `ut_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ut_pwd` text COLLATE utf8_unicode_ci NOT NULL,
  `ut_nom` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ut_prenom` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ut_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `annonce`
--
ALTER TABLE `annonce`
  ADD CONSTRAINT `UT_AN_FOREIGN` FOREIGN KEY (`ut_id`) REFERENCES `utilisateur` (`ut_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `proposition`
--
ALTER TABLE `proposition`
  ADD CONSTRAINT `AN_PR_FOREIGN` FOREIGN KEY (`an_id`) REFERENCES `annonce` (`an_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `UT_PR_FOREIGN` FOREIGN KEY (`ut_id`) REFERENCES `utilisateur` (`ut_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
