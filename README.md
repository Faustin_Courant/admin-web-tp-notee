# Admin web - Tp notée

Polytech Tours : Administration web 

-------------------------------------------------------------------------------------------
Projet final d'évaluation. 
But : 
     Crée une plateforme d'annonce de vente d'objet d'occasion.

Technologie utilisée :
    - Angular
    - Express
    - NodeJs

Date de rendu : 22/02/2022

Auteur : 
     - Faustin Courant
     - Tarik Anvaraly

-------------------------------------------------------------------------------------------

Instruction de mise en marche du service web

Pré-requis : 
     - NodeJs
     - Angular
     - MySQL

Instalation:
     A la racine du projet tapez : 
          npm install 
     Pour installer toutes les dépendances nescessaire au bon fonctionnement de l'application.
     Base de donnée : 
          L'accès a votre base de donnée est configurable (Adresse,User,Password) dans le fichier /Serveur/include/db.js ligne 6 et 7
          Par défault la base de donnée utiliser devra être :
               adresse : localhost
               nom : polycoin 
               user : root 
               mpd :               //Aucun mdp

Lancement : 
     Serveur :
          Dans le repertoire Serveur : 
               npm start

     Client : 
          Dans le repertoire Client-PolyBonCoin :
               ng serve --open
          Une page web devrais alors s'ouvrir sur le site PolyBonCoin


Pour toutes assistance vous pouvez contacter : 
     Faustin Courant : courantfaustin@gmail.com