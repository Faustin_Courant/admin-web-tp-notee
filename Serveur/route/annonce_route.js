/*
 *      Nom du fichier : annonce_route.js
 *      Date : 09/11/2021
 *      Description : Ce fichier contiendra l'API REST 
 *                    permettant de gérer les annonce. 
 *      Fonctionnaliter : 
 *                          Ajouter/Modifier/Retirer/Lister les annonces
 *
 */

const express = require('express');
const db = require('../include/db');
const AnnonceDao = require('../include/dao/annonce');
const { json } = require('express');
const router = express.Router();
const jwt_gest = require('../include/jwt_gestion');

//Permet de récupérer les params contenu dans le body
router.use(express.json());
router.use(express.urlencoded({     // to support URL-encoded bodies
    extended: true
  })); 
//***************************/
// Page GET
//***************************/

//Renvoie une liste JSON de te toutes les annonces
router.get('/', (req, res) => {
    AnnonceDao.getAllAnnonce().then((result,err) => 
    {
      if(err) throw err;
      res.json(result)
    })
});

//Renvoie l'annonce correspondant à l'ID passer en parametre
//Si cette id n'existe pas nous renvoyer une erreur
router.get('/:id', (req, res) => {
    AnnonceDao.getAnnonceById(req.params.id).then((result,err) => {
      if(err) throw err;
      res.json(result);
    }
  );
});

//Renvoie les annonces correspondant à l'utilisateur passer en parametre
//Si cette id n'existe pas nous renvoyer une liste vide
router.get('/ut/:id', (req, res) => {
    AnnonceDao.getAnnonceByUtilisateur(req.params.id).then((result,err) => {
      if(err) throw err;
      res.json(result);
    }
  );
});

router.get('/estAMoi/:id',jwt_gest.authenticateToken, (req, res) => {
  AnnonceDao.verifyEstAMoi(req.params.id,req.user.user.id).then((result,err) => {
    if(err) throw err;
    console.log("RESULTAT : " + result);
    res.json(result);
  }
);
});

//***************************/
// Page POST
//***************************/
router.post('/add',jwt_gest.authenticateToken, (req, res) => {
    if(req.body.titre && req.body.description && req.body.prix)
    {
          let an_to_add = { titre:req.body.titre , description:req.body.description , prix:req.body.prix , utilisateurID : req.user.user.id};
          AnnonceDao.createAnnonce(an_to_add,(result,err) => {
            if(err)throw err;
            if(result == -1 )
            {
              res.status(500).send();
              return;
            }
            res.json(result);
          });
    }
    else
      res.status(422).send();
  });

//***************************/
// Page PUT
//***************************/
router.put('/:id', (req, res) => {
  if(req.body.an_titre && req.body.an_description && req.body.an_prix)
  {
    let an_to_add = {titre:req.body.an_titre , description:req.body.an_description , prix:req.body.an_prix};
    AnnonceDao.updateAnnonce(req.params.id,an_to_add,(result,err) =>{
      if(err) throw err;
        res.json(result);
    })
  }
  else
    res.json(0);
});

//***************************/
// Page DELETE
//***************************/
router.delete('/:id', (req, res) => {
    AnnonceDao.destroyAnnonce(req.params.id,(result,err) => {
      if(err)throw err;
      res.json(result);
    })
});


module.exports = router;