/*
 *      Nom du fichier : Utilisateur_route.js
 *      Date :09/11/2021
 *      Description : Ce fichier contiendra l'API REST 
 *                    permettant de gérer les annonce. 
 *      Fonctionnaliter : 
 *                          Ajouter/Modifier/Retirer/Lister les annonces
 *
 */

const express = require('express');
const db = require('../include/db');
const UtilisateurDao = require('../include/dao/utilisateur');
const router = express.Router();
const bcrypt = require('bcrypt');
const saltRounds = 15;
const jwt_gest = require('../include/jwt_gestion');
//Permet de récupérer les params contenu dans le body
router.use(express.json());
router.use(express.urlencoded({     // to support URL-encoded bodies
    extended: true
  })); 
//***************************/
// Page GET
//***************************/

//Renvoie une liste JSON de te tout les utilisateurs
router.get('/', (req, res) => {
    UtilisateurDao.getAllUtilisateur().then((result,err) => {
        if(err) throw err;
        console.log(result);
        res.json(result);
    })
});

//Renvoie l'utilisateur correspondant à l'ID passer en parametre
//Si cette id n'existe pas nous renvoyer une erreur
router.get('/:id', (req, res) => {
    const query = UtilisateurDao.getUtilisateur(req.params.id).then((result,err) => {
        if(err) throw err;
        console.log(result);
        res.json(result);
    })
});

//Renvoie l'utilisateur correspondant à l'email passer en parametre
//Si cette id n'existe pas nous renvoyer une erreur
router.get('/email/:email', (req, res) => {
    const query = UtilisateurDao.getUtilisateurByMail(req.params.email).then((result,err) => {
        if(err) throw err;
        console.log(result);
        res.json(result);
    })
});

//***************************/
// Page POST
//***************************/
router.post('/add', (req, res) => {
    if(req.body.nom && req.body.prenom && req.body.email && req.body.password && req.body.password_crf)
    {
        if(req.body.password == req.body.password_crf)
        {
            bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
                if(err)throw err;
                let ut_to_add = {nom:req.body.nom,
                    prenom:req.body.prenom,
                    email:req.body.email,
                    password: hash};
                UtilisateurDao.createUtilisateur(ut_to_add,(result, err2) => {
                if (err2) res.status(422).send();
                else res.send();
                }).catch(err => {
                    console.log("ERREUR 2 ici");
                    res.status(422).send();
                }
                );
            });
        }
        else
            res.status(423).send("Mots de passe différents");
    }
    else
        res.status(422).send("Missing parameters");
});

router.post('/login',(req,res) => {
    if(req.body.ut_email && req.body.ut_pwd)
    {
        UtilisateurDao.getPassWordByMail(req.body.ut_email).then((result_ut,err_ut) => {
            if(err_ut) throw err_ut;
            if(result_ut == null){
                res.status(401).send("Invalid login !");
            }
            else
            {
                bcrypt.compare(req.body.ut_pwd, result_ut.password, function(err, result) {
                    console.log("PWD : " +req.body.ut_pwd);
                    console.log("RESULT "+ result_ut.password);
                    if(err) throw err;
                    if(result == false)
                    {
                        res.status(401).send("Invalid password !");
                    }
                    else
                    {
                        console.log("Utilisateur : " + JSON.stringify(result_ut));
                        res.json({accesToken: jwt_gest.generateAccesToken(result_ut), refreshToken : jwt_gest.generateRefreshToken(result_ut),expiresIn : 1800,userID : result_ut.id});
                    }
                });
            }
        })
    }
    else
    {
        res.status(422).send("Missing parameters");
    }
})

router.post("/refreshToken",(req,res) => {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];

    if(!token)
    {
        return res.sendStatus(401);
    }
    jwt.verify(token,process.env.REFRESH_TOKEN_SECRET,(err,user)=>
    {
        if(err)
        {
            return res.sendStatus(401);
        }

        //TODO Check en BDD

        delete user.iat;
        delete user.exp;
        const refreshToken = jwt_gest.generateAccesToken(user);
        res.json({accesToken: refreshToken,expiresIn : 1800});
    })
})
//***************************/
// Page PUT
//***************************/
router.put('/',jwt_gest.authenticateToken , (req, res) => {
    //console.log("USer : " +JSON.stringify(req.user.user.id));
    if(req.body.nom && req.body.prenom && req.body.email)
    {
        //TODO VERIF
        console.log("ICI");
        let ut_to_add = {nom:req.body.nom,prenom:req.body.prenom,email: req.body.email};
        if(req.body.password && req.body.password_crf)
        {
            console.log("MDPMODIF");
            if(req.body.password == req.body.password_crf)
            {
                bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
                    if(err)throw err;
                    ut_to_add.password = hash;
                });
            }
            else
            {
                res.status(423).send("Mots de passe différents");
            }
        }
        
        UtilisateurDao.updateUtilisateur(req.user.user.id,ut_to_add ,(result,err) => {
            if(err) throw err;
            console.log(result);
            res.json(result);
        })
    }
    else
        res.json({erreur:"Param manquant"});
});

//***************************/
// Page DELETE
//***************************/
router.delete('/:id', (req, res) => {
    UtilisateurDao.destroyUtilisateur(req.params.id,(result,err) =>
    {
        if(err) throw err;
        console.log(result);
        res.json(result);
    })
});


module.exports = router;