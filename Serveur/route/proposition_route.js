/*
 *      Nom du fichier : proposition_route.js
 *      Date : 16/11/2021
 *      Description : Ce fichier contiendra l'API REST 
 *                    permettant de gérer les proposition. 
 *      Fonctionnaliter : 
 *                          Ajouter/Modifier/Retirer/Lister les proposition
 *
 */

const express = require('express');
const db = require('../include/db');
const PropositionDao = require('../include/dao/proposition');
const { json } = require('express');
const router = express.Router();
const jwt_gest = require('../include/jwt_gestion');
//Permet de récupérer les params contenu dans le body
router.use(express.json());
router.use(express.urlencoded({     // to support URL-encoded bodies
    extended: true
  })); 
//***************************/
// Page GET
//***************************/

router.get('/:id', (req, res) => {
    PropositionDao.getPropositionById(req.params.id).then((result,err) => 
    {
      if(err) throw err;
      res.json(result)
    })
});

router.get('/ut/:id', (req, res) => {
    PropositionDao.getAllPropositionUtilisateur(req.params.id).then((result,err) => {
      if(err) throw err;
      res.json(result);
    }
  );
});

router.get('/an/:id',jwt_gest.authenticateToken , (req, res) => {
    PropositionDao.getAllPropositionAnnonce(req.params.id,req.user.user.id).then((result,err) => {
      if(err) throw err;
      res.json(result);
    }
  );
});

router.get('/test/:idAn',jwt_gest.authenticateToken , (req, res) => {
  console.log("Anonce utilisateur")
  PropositionDao.getAllPropositionAnnonceUtilisateur(req.params.idAn,req.user.user.id).then((result,err) => {
    if(err) throw err;
    res.json(result);
  }
);
});

router.get("/an/count/:id",jwt_gest.authenticateToken ,(req,res) => {
  PropositionDao.countPropositionByAnnonce(req.params.id,req.user.user.id,req).then((result,err) => {
    if(err) throw err;
    res.json(result);
  });
});

//***************************/
// Page POST
//***************************/
router.post('/add',jwt_gest.authenticateToken ,(req, res) => {
    if(req.body.annonceID && req.body.prix)
    {
          let pr_to_add = { utilisateurID:req.user.user.id , annonceID:req.body.annonceID , prix:req.body.prix , status : -1};
          PropositionDao.createProposition(pr_to_add,(result,err) => {
            if(err)throw err;   
            console.log(result);
            res.json(result);
          });
    }
    else
      res.status(422).send();
  });
  

//***************************/
// Page PUT
//***************************/
router.put('/:id', (req, res) => {
  if(req.body.pr_status && req.body.pr_prix)
  {
    let pr_to_add = {prix:req.body.pr_prix , status:req.body.pr_status};
    PropositionDao.updateProposition(req.params.id,pr_to_add,(result,err) =>{
      if(err) throw err;
        res.json(result);
    })
  }
  else
    res.json(0);
});

router.put('/reponse/:id', (req, res) => {
  if(req.body.reponse )
  {
    let pr_to_add = { status:req.body.reponse};
    PropositionDao.updateProposition(req.params.id,pr_to_add,(result,err) =>{
      if(err) throw err;
        res.json(result);
    })
  }
  else
    res.json(0);
});

//***************************/
// Page DELETE
//***************************/
router.delete('/:id', (req, res) => {
    PropositionDao.destroyProposition(req.params.id,(result,err) => {
      if(err)throw err;
      res.json(result);
    })
});


module.exports = router;