const jwt = require('jsonwebtoken');
require('dotenv').config();

function generateAccesToken(user)
{
    return jwt.sign({user : user},process.env.ACCESS_TOKEN_SECRET, { expiresIn: 1800});
}
module.exports.generateAccesToken = generateAccesToken;

function authenticateToken(req,res,next)
{
    
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];

    if(!token)
    {
        console.log("ERREUR 401 JWT");
        return res.sendStatus(401);
    }
    jwt.verify(token,process.env.ACCESS_TOKEN_SECRET,(err,user)=>
    {
        if(err){
            console.log("Erreur : " +err);
            return res.sendStatus(401);
        }
        req.user = user;
        next();
    })
}
module.exports.authenticateToken = authenticateToken;

function generateRefreshToken(user)
{
    return jwt.sign({user : user},process.env.REFRESH_TOKEN_SECRET, { expiresIn: 3600});
}
module.exports.generateRefreshToken = generateRefreshToken;