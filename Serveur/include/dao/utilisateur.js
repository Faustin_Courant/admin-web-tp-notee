const Op = require('sequelize').Op;
const Utilisateur = require('../models').Utilisateur;

function getAllUtilisateur(callback)
{
    const query = Utilisateur.findAll();
    if(callback)
    {
        return query.then(result=>{callback(result)});
    }
    else
        return query;
}
module.exports.getAllUtilisateur = getAllUtilisateur;

function getUtilisateur(id,callback)
{
    const query = Utilisateur.findByPk(id,{attributes: { exclude: ['password'] }});
    if(callback)
    {
        return query.then(result => {
            callback(result);
        });
    }
    else
        return query;
}
module.exports.getUtilisateur = getUtilisateur;

function getUtilisateurByEmail(email,callback)
{
    const query = Utilisateur.findOne({where: { email: email },attributes: { exclude: ['password'] } });
    if(callback)
    {
        return query.then(result => {
            callback(result);
        });
    }
    else
        return query;
}
module.exports.getUtilisateurByMail = getUtilisateurByEmail;


function getPassWordByMail(email,callback)
{
    const query = Utilisateur.findOne({where: { email: email }});
    if(callback)
    {
        return query.then(result => {
            callback(result);
        });
    }
    else
        return query;
}
module.exports.getPassWordByMail = getPassWordByMail;

function createUtilisateur(utilisateur,callback)
{
    return Utilisateur.create(utilisateur).then(result => {
        if(callback)
            callback(result);
    }).catch(error => {
        console.log('Error tets: ' + error);
        if(callback)
        callback(result,"Erreur");
    }
    );
}
module.exports.createUtilisateur = createUtilisateur;

function updateUtilisateur(id,utilisateur,callback)
{
    return Utilisateur.update(utilisateur,{where:{id:id}}).then(result => {
        if(callback)
            callback(result > 0);
    });
}
module.exports.updateUtilisateur = updateUtilisateur;

function destroyUtilisateur(id,callback)
{
    return Utilisateur.destroy({where:{id:id}}).then(
        result => {
            if(callback)
                callback(result > 0);
        }
    );
}
module.exports.destroyUtilisateur = destroyUtilisateur;

function existUtilisateur(id,callback)
{   
    return getUtilisateur(id).then(result => {
        if(callback)
            callback(result != null);
    });
}
module.exports.existUtilisateur = existUtilisateur;