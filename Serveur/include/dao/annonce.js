const Op = require('sequelize').Op;
const Annonce = require('../models').Annonce;
const UtilisateurDao = require('./utilisateur');

function getAllAnnonce(callback)
{
    const query = Annonce.findAll();
    if(callback)
    {
        return query.then(result=>{callback(result)});
    }
    else
        return query;
}
module.exports.getAllAnnonce = getAllAnnonce;

function getAnnonceById(id,callback)
{
    const query = Annonce.findByPk(id);
    if(callback)
        return query.then(result => {
            callback(result);
        })
    else
        return query;
}
module.exports.getAnnonceById = getAnnonceById;

function getAnnonceByUtilisateur(id_ut,callback)
{
    const query = Annonce.findAll({where:{utilisateurID:id_ut}});
    if(callback)
    {
        return query.then(result => {
            callback(result);
        })
    }
    else
        return query;

}
module.exports.getAnnonceByUtilisateur = getAnnonceByUtilisateur;

function createAnnonce(annonce,callback)
{
    UtilisateurDao.existUtilisateur(annonce.utilisateurID,(result) =>{
        if(result)
            return Annonce.create(annonce).then( (result,err) =>{
                if(callback)
                    callback(result,err);
            });
        else
        {
            return Promise.resolve(-1).then(result => {
                    if(callback)
                        callback(result);
            })
        }
    });
}
module.exports.createAnnonce = createAnnonce;

function updateAnnonce(id,annonce,callback)
{
    return Annonce.update(annonce,{where:{id:id}}).then((result,err) => {
        if(callback)
            callback(result>0);
    })
}
module.exports.updateAnnonce = updateAnnonce;

function destroyAnnonce(id,callback)
{
    return Annonce.destroy({where:{id:id}}).then(result=>{
        if(callback)
            callback(result >0);
    })
}
module.exports.destroyAnnonce = destroyAnnonce;

function existAnnonce(id,callback)
{   
    return getAnnonceById(id).then(result => {
        if(callback)
            callback(result != null);
    });
}
module.exports.existAnnonce = existAnnonce;


function verifyEstAMoi(idAnnonce,idUtilisateur,callback)
{   
    const query=  Annonce.count({where:{id:idAnnonce,utilisateurID: idUtilisateur}});
    if(callback)
    {
        query.then(result => {
            console.log("COUNT RES : " + result);
            if(callback)
            {
                console.log("CALLBAK");
                callback(result > 0);
            }
        });
    }
    else
        return query;
}
module.exports.verifyEstAMoi = verifyEstAMoi;