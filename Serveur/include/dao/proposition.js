const Op = require('sequelize').Op;
const Proposition = require('../models').Proposition;
const UtilisateurDao = require('./utilisateur');
const AnnocneDao = require('./annonce');
const model = require("../models");

function getPropositionById(id,callback)
{
    const query = Proposition.findByPk(id);
    if(callback)
    {
        return query.then(result => {
            callback(result);
        });
    }
    else
        return query;
}
module.exports.getPropositionById = getPropositionById;

function getAllPropositionAnnonce(idAn,idUt,callback)
{
    const query = Proposition.findAll({where:{annonceID:idAn}});
    if(callback)
    {
        return query.then(result => {
            callback(result);
        });
    }
    else
        return query;
}
module.exports.getAllPropositionAnnonce = getAllPropositionAnnonce;

function getAllPropositionAnnonceUtilisateur(idAn,idUt,callback)
{
    const query = Proposition.findAll({where:{annonceID:idAn,utilisateurID:idUt}});
    if(callback)
    {
        return query.then(result => {
            callback(result);
        });
    }
    else
        return query;
}
module.exports.getAllPropositionAnnonceUtilisateur = getAllPropositionAnnonceUtilisateur;

function getAllPropositionUtilisateur(id,callback)
{
    const query = Proposition.findAll({where:{utilisateurID:id}});
    if(callback)
    {
        return query.then(result => {
            callback(result);
        });
    }
    else
        return query;
}
module.exports.getAllPropositionUtilisateur = getAllPropositionUtilisateur;

function createProposition(proposition,callback)
{
    UtilisateurDao.existUtilisateur(proposition.utilisateurID,(result) =>{
        if(result)
            AnnocneDao.existAnnonce(proposition.annonceID,(result) =>{
                if(result)
                    return Proposition.create(proposition).then( (result,err) => {
                        if(callback)
                            callback(result);
                    });
                else
                {
                    return Promise.resolve(-2).then(result => {
                            if(callback)
                                callback(result);
                    })
                }
            });
        else
        {
            return Promise.resolve(-1).then(result => {
                    if(callback)
                        callback(result);
            })
        }
    });
}
module.exports.createProposition = createProposition;

function updateProposition(id,proposition,callback)
{
    return Proposition.update(proposition,{where:{id:id}}).then(result => {
        if(callback)
            callback(result>0);
    });
}
module.exports.updateProposition = updateProposition;

function destroyProposition(id,callback)
{
    return Proposition.destroy({where:{id:id}}).then(result => {
        if(callback)
            callback(result>0);
    })
}
module.exports.destroyProposition = destroyProposition;


function countPropositionByAnnonce(idAnnonce,idUtilisateur,callback)
{
    return Proposition.count({
                                include:{model: model.Annonce, as : "Annonce" },
                                where:{id:id, 'Annonce.id' : idAnnonce, 'Annonce.utilisateurID' : idUtilisateur}
                            }).then(result => {
        if(callback)
            callback(result);
    })
}
module.exports.countPropositionByAnnonce = countPropositionByAnnonce;