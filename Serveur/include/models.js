const Sequelize = require('sequelize');
const db = require('../include/db');

class Utilisateur extends Sequelize.Model{}
Utilisateur.init({
    id:{
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey:true,
        autoIncrement:true
    },
    email:{
        type:Sequelize.STRING,
        unique : true
    },
    password:{
        type:Sequelize.TEXT
    },
    nom:{
        type:Sequelize.STRING
    },
    prenom:{
        type:Sequelize.STRING
    }
},{sequelize:db,modelName:"utilisateur"});

class Annonce extends Sequelize.Model{}
Annonce.init({
    id:{
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey:true,
        autoIncrement:true
    },
    titre:{
        type:Sequelize.STRING
    },
    description:{
        type:Sequelize.TEXT
    },
    prix:{
        type:Sequelize.DOUBLE
    },
    utilisateurID:{
        type:Sequelize.INTEGER,
        model: Utilisateur,
        key: 'id'
    }
},{sequelize:db,modelName:"annonce"});

class Proposition extends Sequelize.Model{}
Proposition.init({
    id:{
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey:true,
        autoIncrement:true
    },
    utilisateurID:{
        type:Sequelize.INTEGER,
        model: Utilisateur,
        key: 'id'
    },
    annonceID:{
        type:Sequelize.INTEGER,
        model: Annonce,
        key: 'id'
    },
    prix:{
        type:Sequelize.DOUBLE
    },
    status:{
        type:Sequelize.BOOLEAN
    }
},{sequelize:db,modelName:"proposition"});




Utilisateur.hasMany(Annonce,{
    foreignKey: 'utilisateurID'
  });

Utilisateur.hasMany(Proposition,{
    foreignKey: 'utilisateurID'
  });
Annonce.hasMany(Proposition,{
    foreignKey: 'annonceID'
  });


module.exports.Utilisateur = Utilisateur;
module.exports.Annonce = Annonce;
module.exports.Proposition = Proposition;


db.sync();