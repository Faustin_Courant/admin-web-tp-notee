/*
 *      Nom du fichier : index.js
 *      Date : 09/11/2021
 *      Description : Ce fichier est le main du serveur 
 *
 */

//Include
const express = require('express');
const cors = require('cors');
const db = require('./include/db');


const annonceRoutes = require('./route/annonce_route');
const utilisateurRoutes = require('./route/utilisateur_route');
const propositionRoutes = require('./route/proposition_route');


//Fin Include

//Paramètre
const port = 8080;

//Fin paramètre




//Configuration du serveur
    const app = express();
    
    app.use(cors());
    app.use("/annonce",annonceRoutes);
    app.use("/utilisateur",utilisateurRoutes);
    app.use("/proposition",propositionRoutes);
    app.use(express.json());
// Fin configuration du serveur

//Démarage du serveur
    app.listen(port, () => {
        console.log(`Le serveur à démarrer sur le port : ${port}`);
    })
//Fin démarage du serveur



